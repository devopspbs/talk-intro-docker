# Primeiros Passos Com Docker

----  ----

## Whoami

- **Dhony Silva**
  - Analista de Dados

- **Tiago Rocha**
  - Engenheiro de Operações

----  ----

## Perguntas Que Vamos Tentar Responder

- O que é docker e porquê é importante?
- Como obter e instalar o docker?
- O que preciso saber para começar a usar?
- Posso ver funcionando?
- Onde posso aprender mais sobre Docker?

----  ----

## O que é docker e porquê é importante?

----

<img src="images/homepage-docker-logo.png" alt="Docker Logo" width="300">

Container Platform para criar, compartilhar e executar com segurança qualquer aplicativo, em qualquer lugar.

----

### O que são containers?

Um container é um ambiente de execução autocontido que compartilha o kernel do sistema host e que é (opcionalmente) isolado dos outros containers desse sistema.

----

<!-- .slide: data-background-image="images/container-vm.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

### Origem do Docker

- Anunciado em 2013 na PyCon de Santa Clara <!-- .element: class="fragment" -->
- Inicialmente foi baseado no Linux Container (LXC) <!-- .element: class="fragment" -->
- Liberado como Open Source em março de 2013 <!-- .element: class="fragment" -->
- Em 2014 já tinha sido adotado por Red Hat e Amazon <!-- .element: class="fragment" -->

----

### Benefícios do fluxo de trabalho do Docker

- Fim do: "mas na minha máquina funciona" <!-- .element: class="fragment" -->
- Controle de versão <!-- .element: class="fragment" -->
- Mesmos artefatos de software em todos os ambientes (devel, teste, homologação e produção) <!-- .element: class="fragment" -->
- Abstração de softwares de hardwares sem sacrificar recursos <!-- .element: class="fragment" -->

----

### O que o Docker não é

- Plataforma de virtualização (VMware, KVM, etc.) <!-- .element: class="fragment" -->
- Plataforma de nuvem (OpenStack, CloudStack, etc.) <!-- .element: class="fragment" -->
- Gerenciamento de configurações (Puppet, Chef, etc.) <!-- .element: class="fragment" -->
- Framework de implantação (Capistrano, Fabric, etc.) <!-- .element: class="fragment" -->
- Ferramenta de gerenciamento de carga (Mesos, Fleet, etc.) <!-- .element: class="fragment" -->
- Ambiente de desenvolvimento (Vagrant, etc.) <!-- .element: class="fragment" -->

----  ----

## Como obter e instalar o docker?

----

### Site oficial

https://docker.com

----

### Instalação

https://docs.docker.com/install/

----

### Docker on Windows & Docker for Windows

----

<!-- .slide: data-background-image="images/docker-windows-vm-linux.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

### Docker Desktop for Windows

https://docs.docker.com/docker-for-windows/install/

----

<!-- .slide: data-background-image="images/d4win-artboard4.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

### Docker Desktop for Mac

https://docs.docker.com/docker-for-mac/install/

----

<!-- .slide: data-background-image="images/d4mac-artboard2.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----  ----

## O que preciso saber para começar a usar?

----

### Docker Image

----

<!-- .slide: data-background-image="images/docker-layers1.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

<!-- .slide: data-background-image="images/docker-filesystems-busyboxrw.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

<img src="images/logo-title-final-registry-2.png" alt="Docker Registry" width="320">

----

### Docker Hub

Biblioteca de Imagens Docker

https://hub.docker.com/

----

### Dockerfile

```docker
FROM alpine:3.8
LABEL maintainer='Tiago Rocha' version='4.4.1'

RUN apk add --quiet --update dhclient && \
    rm -fr /var/cache/apk/*

CMD ["/usr/sbin/dhclient", "-4", "-d"]
```

----

<!-- .slide: data-background-image="images/docker.jpg" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

### Containers Docker são voláteis

----

### Docker Volumes

São o mecanismo preferido para persistir dados gerados e usados ​​pelos containers do Docker.

----

### Docker Network

| NOME   | DRIVER | DESCRIÇÃO                                                                |
|:------:|:------:|:------------------------------------------------------------------------ |
| bridge | bridge | Os containers são associados a uma rede específica.                      |
| host   | host   | Entregar para o container todas as interfaces existentes no docker host. |
| none   | null   | Isola o container para comunicações externas.                            |

----

<img src="images/logo-title-final-compose-2b.png" alt="Docker Compose" width="200">

----

### docker-compose

```docker
version: '3'
services:
  php-apache:
    image: php:7.2.1-apache
    ports:
      - 80:80
    volumes:
      - ./DocumentRoot:/var/www/html:z
    links:
      - 'mariadb'

  mariadb:
    image: mariadb:10.1
      volumes:
        - mariadb:/var/lib/mysql
      environment:
        TZ: "Europe/Rome"
        MYSQL_ALLOW_EMPTY_PASSWORD: "no"
        MYSQL_ROOT_PASSWORD: "rootpwd"
        MYSQL_USER: 'testuser'
        MYSQL_PASSWORD: 'testpassword'
        MYSQL_DATABASE: 'testdb'

volumes:
  mariadb:
```

----  ----

## Posso ver funcionando?

----

### Demo!

Aqui a gente erra ao vivo! ;p

----  ----

## Onde posso aprender mais sobre Docker?

----

### Documentação Oficial

https://docs.docker.com/

----

<img src="https://d2sofvawe08yqg.cloudfront.net/dockerparadesenvolvedores/hero?1549480959" alt="Docker Compose" width="200">

### Livro Docker para Desenvolvedores

https://leanpub.com/dockerparadesenvolvedores

https://github.com/gomex/docker-para-desenvolvedores

----

### Grupo de Docker no Telegram

https://t.me/dockerbr

----

### Play with Docker

https://labs.play-with-docker.com/

----  ----

## Conclusão

Docker é massa... <!-- .element: class="fragment" -->

Mas Docker não é bala de prata! <!-- .element: class="fragment" -->

----  ----

## Obrigado!

----  ----

## Contatos

- **Dhony Silva**

- **Tiago Rocha**
  - **Blog:** [tiagorocha.eti.br](https://tiagorocha.eti.br/)
  - **Telegram:** [@Tiag0Rocha](https://t.me/Tiag0Rocha)
  - **IRC:** tiagorocha (Freenode e OFTC)
